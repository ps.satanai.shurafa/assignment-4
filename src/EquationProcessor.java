import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Stack;
import java.util.regex.*;

public class EquationProcessor {
    private Stack<Double> operands = new Stack<>();
    private Stack<Character> operations = new Stack<>();
    private HashMap<Character,Double> variables = new HashMap<>();
    private HashMap<String,Integer> operationsPriorities = new HashMap<>();
    private String equation,num="";
    private boolean needToReinitialize = true;
    private Double result;


    public EquationProcessor(String equation){
        this.equation = equation;
        checkEquation();
        initPriorities();
        result = 0.0;
        categorise();
    }

    public void initPriorities(){
        operationsPriorities.put(")",5);
        operationsPriorities.put("^",5);
        operationsPriorities.put("*",4);
        operationsPriorities.put("/",3);
        operationsPriorities.put("+",2);
        operationsPriorities.put("-",1);
        operationsPriorities.put("(",0);
    }

    public Integer getPriority(String operation){
        return operationsPriorities.get(operation);
    }

    public boolean isOperation(Character character){
        return Pattern.matches("[+/*^-]", character.toString());
    }

    public boolean isVariable(Character character){
        return Pattern.matches("[aA-zZ]", character.toString());
    }

    public boolean isNumber(Character character){
        return Pattern.matches("[0-9]", character.toString());
    }

    public boolean isBraces(Character character) {
        return Pattern.matches("[()]", character.toString());
    }

    public void pushToOperations(Character character){
        operations.push(character);
    }

    public void pushToOperands(Double value){
        operands.push(value);
    }

    public boolean haveValue(Character character){
        return variables.containsKey(character);
    }

    public void addNewVariable(Character character, Double value){
        variables.put(character,value);
    }

    public Double getValue(Character character){
        return variables.get(character);
    }

    public Double calculate(Character operation, Double firstValue, Double secondValue){
        switch (operation){
            case '^':
                return Math.pow(secondValue,firstValue);
            case '*':
                return firstValue*secondValue;
            case '/':
                if (firstValue == 0)
                    throw new ArithmeticException ( "Cannot divide by zero");

                return secondValue/firstValue;
            case '+':
                return firstValue+secondValue;
            case '-':
                return secondValue-firstValue;
        }
        return 0.0;
    }

    public void doCalculation(){
        Double firstValue = operands.peek();
        operands.pop();
        Double secondValue  = operands.peek();
        operands.pop();
        Character operation = operations.peek();

        operations.pop();
        result = calculate(operation,firstValue,secondValue);
        pushToOperands(result);
    }

    public boolean isLastChar(Integer index){
        return index == equation.length();
    }

    public String checkIfNextIsNum(Integer index){
        if(needToReinitialize){
            num = "";
            needToReinitialize = false;
        }
        if(isNumber(equation.charAt(index))){
            num += equation.charAt(index);
            if(!isLastChar(index+1)){
                checkIfNextIsNum(index+1);
            }
        }
        else {
            needToReinitialize = true;
        }
        return num;
    }

    public void checkPriority(Character operation){
        Integer previousPriority = getPriority(operations.peek().toString());
        Integer currentPriority = getPriority(Character.toString(operation));

        if(currentPriority < previousPriority){
            doCalculation();
        }
    }

    public void categorise() {
        boolean previousIsOperation = false, previousIsVar = false;
        Integer leftBrace, rightBrace = 0;
        Scanner sc= new Scanner(System.in);
        Character character;
        Double value = 0.0;

        for(int i=0; i<equation.length(); i++){
            character = equation.charAt(i);

            if(isOperation(character)){
                if(previousIsOperation){
                    throw new IncorrectEquationException();
                }
                if(!operations.isEmpty() && !operations.peek().equals('(')) {
                    checkPriority(character);
                }

                pushToOperations(character);
                previousIsVar = false;
                previousIsOperation = true;
            }
            else if (isVariable(character)) {

                if(previousIsVar){
                    throw new IncorrectEquationException();
                }
                else if(!haveValue(character)){
                    value = sc.nextDouble();
                    addNewVariable(character,value);
                }
                else if (haveValue(character)) {
                    value = getValue(character);
                }

                pushToOperands(value);
                previousIsVar = true;
                previousIsOperation = false;
            }
            else if(isNumber(character)){
                String num = checkIfNextIsNum(i);
                Integer digits = num.length();
                value = Double.valueOf(num);

                i += digits-1;

                pushToOperands(value);
                previousIsVar = false;
                previousIsOperation = false;
            }
            else if(isBraces(character)){
                if(character.equals( '(')){
                    pushToOperations(character);
                }
                else if (character.equals(')')){

                    while (!operations.peek().equals('(')){
                        doCalculation();
                    }
                    operations.pop();
                }
            }
            else {
                throw new IncorrectEquationException();
            }

        }

        while (!operations.isEmpty()){
            doCalculation();
        }
        result = operands.peek();
    }

    public Double getResult(){
        return result;
    }

    public void checkEquation() {
        if(equation == null){
            throw new EmptyStackException();
        }
        else if(Pattern.matches("^[+)/*-]{1,}[aA-zZ 0-9 +^/*()-]+", equation) ||
                Pattern.matches("[aA-zZ 0-9]{1}[aA-zZ 0-9 +^/*()-]+[+(/*-]${1,}", equation) ||
                Pattern.matches("[(+/*^)-]{0,}[0-9 aA-zZ]{0,}[(+/*^)-]{0,}", equation)){
            throw new IncorrectEquationException();
        }

    }

    public static void main(String[] args){

       EquationProcessor e = new EquationProcessor("(1+1)*2+(2-1)");
       System.out.println(e.getResult());

    }

}
